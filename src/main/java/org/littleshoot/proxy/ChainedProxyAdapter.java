package org.littleshoot.proxy;

import io.netty.handler.codec.http.HttpObject;

import java.net.InetSocketAddress;

import javax.net.ssl.SSLEngine;

/**
 * Convenience base class for implementations of {@link ChainedProxy}.
 */
public class ChainedProxyAdapter implements ChainedProxy {
    /**
     * {@link ChainedProxy} that simply has the downstream proxy make a direct
     * connection to the upstream server.
     */
    public static ChainedProxy FALLBACK_TO_DIRECT_CONNECTION = new ChainedProxyAdapter();

    public InetSocketAddress getChainedProxyAddress() {
        return null;
    }

    public InetSocketAddress getLocalAddress() {
        return null;
    }

    public TransportProtocol getTransportProtocol() {
        return TransportProtocol.TCP;
    }

    public boolean requiresEncryption() {
        return false;
    }

    public SSLEngine newSslEngine() {
        return null;
    }
    
    public void filterRequest(HttpObject httpObject) {
    }
    
    public void connectionSucceeded() {
    }

    public void connectionFailed(Throwable cause) {
    }

    public void disconnected() {
    }

    public SSLEngine newSslEngine(String peerHost, int peerPort) {
        return null;
    }
}
