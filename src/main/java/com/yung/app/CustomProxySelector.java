package com.yung.app;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * So the way a ProxySelector works is that for all Connections made,
 * it delegates to a proxySelector(There is a default we're going to
 * override with this class) to know if it needs to use a proxy
 * for the connection.
 * <p>This class was specifically created with the intent to proxy connections
 * going to the allegiance soap service.</p>
 * This class contain a PAC map key[domain]/value[proxy list] to decide which proxy should be used.
 *
 * @author Nate, revised by Yung-Long Li
 */
public class CustomProxySelector extends ProxySelector {

    private static final ProxySelector original;
    private boolean debug = false;

    private static final Logger logger = Logger.getLogger(CustomProxySelector.class.getName());

    private Map<String, List<Proxy>> proxyPACMap = new ConcurrentHashMap<String, List<Proxy>>();
    
    static {
        original = ProxySelector.getDefault();
    }
    
    /*
     * We want to hang onto the default and delegate
     * everything to it unless it's one of the url's
     * we need proxied.
     */
    public CustomProxySelector() {
    }
    
    /**
     * change JDK default ProxySelector
     * 
     * @param customProxySelector custom ProxySelector
     */
    public static void setJDKDefaultProxySelector(CustomProxySelector customProxySelector) {
        ProxySelector.setDefault(customProxySelector);
    }
    
    /**
     * change JDK default ProxySelector
     * 
     * @param customProxySelector
     */
    public static void turnOnProxy(CustomProxySelector customProxySelector) {
        ProxySelector.setDefault(customProxySelector);
    }
    
    /**
     * Rollback to original JDK default ProxySelector
     * 
     */
    public static void rollbackJDKDefaultProxySelector() {
        ProxySelector.setDefault(original);
    }
    
    /**
     * Rollback to original JDK default ProxySelector
     */
    public static void turnOff() {
        ProxySelector.setDefault(original);
    }

    @Override
    public List<Proxy> select(URI uri) {
        if (uri == null) {
            throw new IllegalArgumentException("URI can't be null.");
        }
        String lowerURI = uri.getHost().toLowerCase();
        if (proxyPACMap.containsKey("*")) {
            List<Proxy> list = proxyPACMap.get("*");
            if (debug) {
                logger.info("We're trying to reach " + uri + "[" + lowerURI + "] so we're going to use the extProxy." + list);
            }
            return list;
        }
        if (proxyPACMap.containsKey(lowerURI)) {
            List<Proxy> list = proxyPACMap.get(lowerURI);
            if (debug) {
                logger.info("We're trying to reach " + uri + "[" + lowerURI + "] so we're going to use the extProxy." + list);
            }
            return list;
        }
        return original.select(uri);
    }

    /*
    * Method called by the handlers when it failed to connect
    * to one of the proxies returned by select().
    */
    @Override
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        if (uri == null || sa == null || ioe == null) {
            throw new IllegalArgumentException("Arguments can't be null.");
        }
        if (debug) logger.severe("Failed to connect to a proxy when connecting to " + uri.getHost() + ", " + sa.toString() + ", " + ioe.toString());
    }

    /**
     * add host for proxy
     * 
     * @param host host name
     * @param ip ip
     * @param proxyPort port
     */
    public void addProxy(String host, String ip, String proxyPort) {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ip, (null == proxyPort) ? 80 : Integer.valueOf(proxyPort)));
        List<Proxy> list = proxyPACMap.get(host);
        if (list == null) {
            list = new ArrayList<Proxy>();
        }
        if (!checkExist(proxy, list)) {
            list.add(proxy);
        }
        proxyPACMap.put(host, list);
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
    private boolean checkExist(Proxy proxy, List<Proxy> list) {
        if (proxy == null || list == null || list.size() == 0) {
            return false;
        }
        String checkProxy = proxy.toString();
        for (Proxy p : list) {
            if (checkProxy.equalsIgnoreCase(p.toString())) {
                return true;
            }
        }
        return false;
    }
    
}