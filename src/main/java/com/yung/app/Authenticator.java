package com.yung.app;

import org.littleshoot.proxy.ProxyAuthenticator;

public class Authenticator implements ProxyAuthenticator {
    
    private String userName = "";
    
    private String password = "";
    
    public Authenticator(){
    }
    
    public Authenticator(String useuserNamername, String password) {
        this.setUserName(userName);
        this.setPassword(password);
    }
    
    public boolean authenticate(String userName, String password) {
        if (this.userName.equals("") && this.password.equals("")) {
            return true;
        }
        if (this.userName.equals(userName) && this.password.equals(password)) {
            return true;
        }
        return false;
    }

    public String getRealm() {
        return null;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
