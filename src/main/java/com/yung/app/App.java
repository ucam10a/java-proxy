package com.yung.app;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.InetSocketAddress;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

public class App extends JPanel implements ActionListener {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public static final String NEW_LINE = System.getProperty("line.separator");
    
    private static final AppLogger logger = AppLogger.getLogger(App.class);
    private HttpProxyServer server;
    private JButton startButton, stopButton, certButton;
    protected JScrollPane scrollPane;
    private JTextField userField, pwdField, portField, ipField, proxyIpField, proxyPortField;
    protected JTextArea log;
    private CustomProxySelector proxySelector;
    private String currentDir = "";
    private JCheckBox trace;
    
    public App() {
        super(new BorderLayout());
        init();
    }
    
    public void init() {
        log = new JTextArea(30, 35);
        log.setMargin(new Insets(5, 5, 5, 5));
        log.setEditable(false);
        scrollPane = new JScrollPane(log);
        
        startButton = new JButton("start", null);
        startButton.addActionListener(this);
        
        stopButton = new JButton("stop", null);
        stopButton.addActionListener(this);
        
        certButton = new JButton("SSLCert", null);
        certButton.addActionListener(this);
        
        //JLabel userLabel = new JLabel("user");
        userField = new JTextField("", 8);
        
        //JLabel pwdLabel = new JLabel("pwd");
        pwdField = new JTextField("", 8);
        
        JLabel ipLabel = new JLabel("listen ip");
        ipField = new JTextField("", 12);
        
        JLabel portLabel = new JLabel("port");
        portField = new JTextField("", 6);
        
        trace = new JCheckBox();
        trace.setText("trace URI");
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new FlowLayout());
        //textPanel.add(userLabel);
        //textPanel.add(userField);
        //textPanel.add(pwdLabel);
        //textPanel.add(pwdField);
        textPanel.add(ipLabel);
        textPanel.add(ipField);
        textPanel.add(portLabel);
        textPanel.add(portField);
        textPanel.add(trace);
        
        JLabel proxyIpLabel = new JLabel("JDK Proxy ip");
        proxyIpField = new JTextField("", 12);
        
        JLabel proxyPortLabel = new JLabel("port");
        proxyPortField = new JTextField("", 6);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(startButton);
        buttonPanel.add(stopButton);
        buttonPanel.add(certButton);
        
        JPanel proxyPanel = new JPanel();
        proxyPanel.add(proxyIpLabel);
        proxyPanel.add(proxyIpField);
        proxyPanel.add(proxyPortLabel);
        proxyPanel.add(proxyPortField);
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
        topPanel.add(textPanel);
        topPanel.add(proxyPanel);
        topPanel.add(buttonPanel);
        
        //Add the buttons and the log to this panel.
        add(topPanel, BorderLayout.PAGE_START);
        add(scrollPane, BorderLayout.CENTER);
        logMessage(scrollPane, log, "please input your machine ip to listen ip and port!");
        logMessage(scrollPane, log, "otherwise it will only listen on 127.0.0.1:9090!");
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == startButton) {
            Authenticator auth = new Authenticator();
            String user = userField.getText();
            String pwd = pwdField.getText();
            //logMessage(scrollPane, log, "user: '" + user + "', pwd: '" + pwd + "'");
            auth.setUserName(user);
            auth.setPassword(pwd);
            String ip = ipField.getText();
            if (ip == null || "".equals(ip)) {
                ip = "127.0.0.1";
            }
            String portStr = portField.getText();
            int port = 9090;
            try {
                port = Integer.valueOf(portStr);
            } catch (Exception ex) {
            }
            logMessage(scrollPane, log, "listen on: " + ip + ":" + port);
            InetSocketAddress address = new InetSocketAddress(ip, port);
            server = DefaultHttpProxyServer.bootstrap().withAddress(address).withAllowLocalOnly(false).start();
            if (trace.isSelected()) {
                server.setEanbleTrace(true);
            } else {
                server.setEanbleTrace(false);
            }
            //server = DefaultHttpProxyServer.bootstrap().withProxyAuthenticator(auth).withPort(port).start();
            logMessage(scrollPane, log, "start proxy");
            
            String proxyIp = proxyIpField.getText();
            String proxyPort = proxyPortField.getText();
            if (proxyIp != null && !"".equals(proxyIp)) {
                if (proxySelector == null) {
                    proxySelector = new CustomProxySelector();
                }
                proxySelector.addProxy("*", proxyIp, proxyPort);
                CustomProxySelector.setJDKDefaultProxySelector(proxySelector);
                logMessage(scrollPane, log, "use proxy on " + proxyIp + ":" + proxyPort);
            }
        
        }
        if (e.getSource() == stopButton) {
            stopProxy();
        }
        if (e.getSource() == certButton) {
            JFileChooser fc = new JFileChooser();
            String temp = new File(currentDir).getAbsolutePath();
            File dir = new File(temp);
            fc.setCurrentDirectory(dir);
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File f = fc.getSelectedFile();
                String certPath = f.getAbsolutePath();
                System.setProperty("javax.net.ssl.trustStore", certPath);
                logMessage(scrollPane, log, "setup certificate on " + certPath);
            }
        }
    }
    
    private void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Proxy App");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(this);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        // add close event
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                stopProxy();
            }
        });
        
        // set icon
        InputStream in = App.class.getClassLoader().getResourceAsStream("icon.gif");
        byte[] bytes = toByteArray(in);
        Image icon = Toolkit.getDefaultToolkit().createImage(bytes);
        frame.setIconImage(icon);
        
    }

    protected void stopProxy() {
        if (server != null) {
            logMessage(scrollPane, log, "stop proxy");
            server.stop();    
        }
        if (proxySelector != null) {
            CustomProxySelector.rollbackJDKDefaultProxySelector();
        }
    }

    private byte[] toByteArray(InputStream in) {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = in.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            return buffer.toByteArray();    
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
    
    public static void logMessage(JScrollPane scrollPane, JTextArea log, String message) {
        JScrollBar vertical = scrollPane.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
        log.append(message + NEW_LINE);
        logger.info(message);
    }
    
    public static void main(String args[]) throws Exception {
        
        App app = new App();
        app.run();
        
    }
    
}